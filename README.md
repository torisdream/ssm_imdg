# **Synopsis** #

Greenland is the leading open source In-Memory Data Grid. We provide comfortable interface that uses data structure.

# **Setting for JVM** #
1. make policy file for your servers

example)

grant {  
permission java.security.AllPermission "", "";  
};

2. setting jvm argument for running application


-Djava.security.manager -Djava.security.policy={your_policy_file_path} -XX:MaxDirectMemorySize={your_maximum_memory_for_greenland}M -Xms{quarter_of_your_memory_for_greenland}M -Xmx{your_maximum_heap_memory_for_greenland}M -XX:PermSize={1/8_of_your_memory_for_greenland}m -XX:MaxPermSize={quarter_of_your_memory_for_greenland}m -XX:NewSize={1/8_of_your_memory_for_greenland}m

# **serverinfo.xml setting** #

![캡처.PNG](https://bitbucket.org/repo/EaKppk/images/454014062-%EC%BA%A1%EC%B2%98.PNG)

# **RMI Stub generation ** #
Move to your compiled file(*.class files) root folder and type this command in CLI.



rmic org.secmem.greenland.remote.CommandBuilderImpl

rmic org.secmem.greenland.remote.InstanceConnectionImpl

rmic org.secmem.greenland.remote.InstanceManagerImpl

rmiregistry

and running your service

#  **Installation** #

Download full source code and deploy / create RMI stub.

# **License** #

WE FOLLOW **MIT LICENSE**.