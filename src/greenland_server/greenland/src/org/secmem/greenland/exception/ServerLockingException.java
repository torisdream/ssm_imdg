package org.secmem.greenland.exception;

public class ServerLockingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String serverAddr;
	
	public ServerLockingException(String serverAddr){
		super();
		this.serverAddr = serverAddr; 
	}
	
	@Override
	public String getMessage(){
		return serverAddr + " is Locked";
	}

}
