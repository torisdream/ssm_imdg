package org.secmem.greenland.io;

import java.util.HashMap;

public class SimpleMemoryAllocator {
	private static SimpleMemoryAllocator instance;
	private HashMap<String, SimpleManagedData> dataList;
	
	public static SimpleMemoryAllocator getInstance(){
		if(instance != null)
			return instance;
		else{
			instance = new SimpleMemoryAllocator();
			return instance;
		}
	}
	
	public SimpleMemoryAllocator(){
		dataList = new HashMap<String, SimpleManagedData>();
	}
	
	public int insertData(String key, int dataType, String data){
		if(dataList.containsKey(key)){
			dataList.get(key).dataType = dataType;
			dataList.get(key).data = data;
			
			return 2;
		}else{
			SimpleManagedData temp = new SimpleManagedData();
			temp.dataType = dataType;
			temp.data = data;
			dataList.put(key, temp);
			
			return 1;
		}
	}
	
	public String getData(String key){
		if(dataList.containsKey(key)){
			return dataList.get(key).data;
		}else{
			return null;
		}
	}
	
	public String removeData(String key){
		if(dataList.containsKey(key)){
			String ret = dataList.get(key).data;
			dataList.remove(key);
			return ret;
		}else{
			return null;
		}
	}
	
	public Boolean checkData(String key){
		return dataList.containsKey(key);
	}
	
	
}
