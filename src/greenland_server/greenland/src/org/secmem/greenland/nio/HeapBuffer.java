package  org.secmem.greenland.nio;

import java.nio.ByteBuffer;

public class HeapBuffer {
	ByteBuffer HeapBuffer;
	
	public HeapBuffer() {
		this.HeapBuffer = ByteBuffer.allocate(1024*1024*1024);	//1GB
	}
	
	public HeapBuffer(int size) {
		this.HeapBuffer = ByteBuffer.allocate(size);
	}
	
	public HeapBuffer compact() {
		HeapBuffer.compact();
		return this;
	}
	
	public byte get() {
		return HeapBuffer.get();
	}
	
	public byte get(int index) {
		return HeapBuffer.get(index);
	}
	
	public void put(byte b) {
        ensureSize(1);
        HeapBuffer.put(b);
    }
	
	public void put(byte[] b) {
		ensureSize(b.length);
		HeapBuffer.put(b);
		//�������� ����� Ȯ�� ���ִ� �� �غ��ߴ�
	}
	
	public void put(byte[] b, int pos) {
		ensureSize(b.length);
		HeapBuffer.position(pos);		//set Position
		HeapBuffer.put(b);
		//�������� ����� Ȯ�� ���ִ� �� �غ��ߴ�
	}
	
	private boolean ensureSize(int i) {
        check();
        if (HeapBuffer.remaining() < i) {
            return true;
        }else {
        	return false;
        }
    }
	
//	private void ensureSize(int i) {
//        check();
//        if (HeapBuffer.remaining() < i) {
//            int newCap = Math.max(HeapBuffer.limit() << 1, HeapBuffer.limit() + i);
//            ByteBuffer newBuffer = HeapBuffer.isHeap() ? ByteBuffer.allocateHeap(newCap) : ByteBuffer.allocate(newCap);
//            newBuffer.order(HeapBuffer.order());
//            HeapBuffer.flip();
//            newBuffer.put(HeapBuffer);
//            HeapBuffer = newBuffer;
//        }
//    }
	
	private void check() {
        if (HeapBuffer == null) {
            throw new IllegalStateException("Buffer is closed!");
        }
    }
}

class HeapBufferThread implements Runnable {
	HeapBuffer HeapBuf;
	
	public void run() {
		HeapBuf = new HeapBuffer();
	}
	
}