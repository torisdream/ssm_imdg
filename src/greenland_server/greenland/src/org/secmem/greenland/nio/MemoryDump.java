package org.secmem.greenland.nio;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.ArrayList;

public class MemoryDump {
	MemoryManager mmg;

	public MemoryDump(MemoryManager mmg) {
		this.mmg = mmg;
	}

	/**
	 * executeLFU 함수 구현
	 * execute LFU alg. make memory dump file.
	 * 
	 * @param int useCount
	 * @return void
	 * @author 송원근
	 */
	public void executeLFU(int useCount) {
		ManagedData md;
		int tempCount = 0;
		ArrayList<ManagedData> removeMdList = new ArrayList<ManagedData>();

		BufferedOutputStream bw;
		byte[] byteArray;
		try {
			bw = new BufferedOutputStream(new FileOutputStream("C:\\memory.dmp", false));
			
			Iterator<ManagedData> it = mmg.getMdList().iterator();
			while(it.hasNext()) {
				md = it.next();
				tempCount = md.getUseCount();

				// Soreted Set은 directBuffer를 사용하지 않으므로 처리안함
				if ( !(tempCount > useCount) && (md.getType() != 2) ) {
					//direct Buffer에서 value를 del
					byteArray = mmg.del(md.getPos(), md.getBufferSize());
					
//					//LFUManager 에 value를 추가하여 객체생성
//					os.writeObject(new LFUManagedData(md, byteArray));
//					//객체를 바이트 배열로 변환
//					byteArray = out.toByteArray();
					
					byteArray = toByteArray(new LFUManagedData(md, byteArray));
					
					//add
					mmg.getMemoryDumpLengthList().add(byteArray);
					
					//file write
					bw.write(byteArray);
					bw.flush();
					
					/* 동기화 에러가 나서 removeMdList에 넣어주고 나중에 한번에 remove
					//mdList에서 remove
					mmg.getMdList().remove(md);
					*/
					removeMdList.add(md);
					
					if(md.getType() == 0) {
						//keys 에서 remove
						mmg.getKeys().remove(md.getKey());
					} else if(md.getType() == 1) {
						/* hashes 는 LFU에서 뺌
						//hashes의 value의 hashMap에서 remove
						mmg.getHashes().get(md.getKey()).remove(md);
						//지운 후 value가 없다면 hashed에서 remove
						if(mmg.getHashes().get(md.getKey()).size() == 0) {
							mmg.getHashes().remove(md.getKey());
						}
						*/
					}
				}
			}
			
			//removeMdList 를 이용하여 mdList 에서 remove
			it = removeMdList.iterator();
			while(it.hasNext()) {
				md = it.next();
				mmg.getMdList().remove(md);
				//memoryDumpKeyList 에 add
				mmg.getMemoryDumpKeyList().add(md.getKey());
			}
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * readMemoryDmp 함수 구현
	 * read memory dump file
	 * 
	 * @param String filename
	 * @return void
	 * @author 송원근
	 */
	public void readMemoryDmp(String filename) {
		BufferedInputStream bis;
		LFUManagedData lMd;
		ManagedData md;
//		filename = "C:\\memory.dmp";
		try {
			bis = new BufferedInputStream(new FileInputStream(filename));
			for(int i=0; i < mmg.getMemoryDumpLengthList().size(); i++) {
				//memoryDumpList 에 있는 값으로 바이트 배열 생성
				byte[] data = new byte[mmg.getMemoryDumpLengthList().get(i).length];
				//만든 data 바이트 배열에 인덱스 0부터 length만큼 읽어 들여서 넣음
				bis.read(data, 0, mmg.getMemoryDumpLengthList().get(i).length);
				try {
					//data 바이트 배열을 객체로 역직렬화
					lMd = (LFUManagedData)toObject(data);
					System.out.println(lMd.toString());
					
					md = lMd.returnManagedData();
					//directBuffer에 put 하기전에 pos으로 ManagedData 생성
					md.setPos(mmg.position());
					//directBuffer에 put
					mmg.put(lMd.getValue(),mmg.position());
					mmg.getMdList().add(md);
					
					if(md.getType() == 0) {
						//만약에 keys에 메모리가 내려간후에 키가 올라간거라면 키 갱신 안됨.
						mmg.getKeys().put(md.getKey(), md);
					} else if(md.getType() == 1) {
						//hashes는 안함
					}
					
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * toByteArray 함수 구현
	 * Object to byte[], Serialization
	 * 
	 * @param Object value
	 * @return byte[]
	 * @author 송원근
	 */
	public byte[] toByteArray(Object value) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ObjectOutputStream os = new ObjectOutputStream(out);
		os.writeObject(value);
		return  out.toByteArray();
	}
	
	/**
	 * toObject 함수 구현
	 * byte[]  to Object, Deserialization
	 * 
	 * @param byte[]
	 * @return Object
	 * @author 송원근
	 */
	public Object toObject (byte[] bytes) throws IOException, ClassNotFoundException {
		ByteArrayInputStream bis = new ByteArrayInputStream (bytes);
		ObjectInputStream ois = new ObjectInputStream (bis);
		return ois.readObject();
	}

}
