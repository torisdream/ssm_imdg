package org.secmem.greenland.core;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.RMISocketFactory;

import javax.xml.parsers.ParserConfigurationException;

import org.secmem.greenland.config.GreenlandConfig;
import org.secmem.greenland.config.ServerInfo;
import org.secmem.greenland.message.MessageManager;
import org.secmem.greenland.nio.MemoryManager;
import org.secmem.greenland.pool.ConnectionPoolManager;
import org.secmem.greenland.remote.CommandBuilderImpl;
import org.secmem.greenland.remote.InstanceConnection;
import org.secmem.greenland.remote.InstanceConnectionImpl;
import org.secmem.greenland.remote.InstanceManager;
import org.secmem.greenland.remote.InstanceManagerImpl;
import org.secmem.greenland.statemachine.GreenlandStateMachine;
import org.xml.sax.SAXException;

public class GreenlandEngine {
	private static InstanceManager instanceManager;
	public static void main(String args[]) throws SAXException, IOException, ParserConfigurationException, NotBoundException{
		
		try {
			RMISocketFactory.setSocketFactory( new RMISocketFactory()
			{
			    public Socket createSocket( String host, int port )
			        throws IOException
			    {
			        Socket socket = new Socket();
			        socket.setSoTimeout( 6000 );
			        socket.setSoLinger( false, 0 );
			        socket.connect( new InetSocketAddress( host, port ), 6000 );
			        return socket;
			    }

			    public ServerSocket createServerSocket( int port )
			        throws IOException
			    {
			        return new ServerSocket( port );
			    }
			} );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//addHooker
		Runtime.getRuntime().addShutdownHook(new Thread(){
			@Override
			public void run(){
				//lfu file remove
				//file backup
				//many many things...
				System.out.println("greenland successfully exited :) see you later!");
			}
		});
		
		//Message Manager
		MessageManager message = MessageManager.getInstance();
		
		MessageManager.getInstance().insertMessage("Greenland IMDG server service start ... ");
		MessageManager.getInstance().insertMessage("This server adress is "+InetAddress.getLocalHost().getHostAddress() + " ... ");
		MessageManager.getInstance().insertMessage("Start read serverinfo.xml file ... ");
		GreenlandConfig conf = GreenlandConfig.getInstance();
		MessageManager.getInstance().insertMessage("complete read serverinfo.xml file...");
		//restore strategy

		
		//memory allocator
		MemoryManager primary = new MemoryManager(1024 * 1024 * conf.getLocalServerInfo().getSize());

		
		//start RMI service
		MessageManager.getInstance().insertMessage("start RMI service ... ");
		instanceManager = new InstanceManagerImpl(InetAddress.getLocalHost());
		
		GreenlandStateMachine.createInstance(primary, instanceManager);
		
		InstanceConnection conn = new InstanceConnectionImpl();

		conn.setServerInfo(conf.getLocalServerInfo());
		conn.setMessageManager(message);
		conn.setCommandBuilder(new CommandBuilderImpl());
		instanceManager.addInstance(conn);		
		
		
		Naming.rebind("GreenlandInstanceConnection", GreenlandStateMachine.getInstance().getInstanceManager().getLocalInstance());
		
		GreenlandStateMachine.getInstance().getInstanceManager();	
		
		MessageManager.getInstance().insertMessage("Start binding server ... ");
		
		new Thread(new ConnectionPoolManager(GreenlandStateMachine.getInstance().getInstanceManager())).start();
		new Thread(new MemoryCheckerThread()).start();
	}
		
		
}

class MemoryCheckerThread extends Thread{
	public void run(){
		try {
			while(true){
				ServerInfo s = GreenlandStateMachine.getInstance().getInstanceManager().getLocalInstance().getServerInfo();
				MemoryManager m = GreenlandStateMachine.getInstance().getMemoryManager();
				s.setPrimaryKeyCount(m.getMdList().size());
				s.setPirmaryMemoryUsage(m.getUsingMemorySize());
				s.setBackupKeyCount(m.getMdBackUpList().size());
				s.setBackupMemoryUsage(m.getUsingBackUpMemorySize());
				
				if((m.getUsingMemorySize() / (s.getSize()*1024*1024.0)) >= 0.8 ){
					GreenlandStateMachine.getInstance().getInstanceManager().getLocalInstance().startLocking();
					GreenlandStateMachine.getInstance().getInstanceManager().getLocalInstance().defragment();
					GreenlandStateMachine.getInstance().getInstanceManager().getLocalInstance().releaseLocking();
				}
				
				Thread.sleep(1000);
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
