package org.secmem.greenland.datastructure;

import java.io.IOException;
import java.util.Calendar;

import org.secmem.greenland.message.MessageManager;
import org.secmem.greenland.nio.ManagedData;
import org.secmem.greenland.nio.MemoryManager;

public class KeysProcess {
	private MemoryManager mmg; 
	private long timeStamp;
	private ExpireManagerThread t1;
	
	public KeysProcess(MemoryManager mmg) {
		this.mmg = mmg;
		t1 = new ExpireManagerThread();
		t1.start();
	}
	
	/**
	 * Keys EXISTS 함수 구현
	 * 
	 * @param String key
	 * @return Boolean
	 * @author 송원근
	 */
	public Boolean EXISTS(String key) {
//		return mmg.getKeys().containsKey(key);
		return (mmg.getKeys().containsKey(key)) || (mmg.getMemoryDumpKeyList().contains(key));
	}
	
	/**
	 * Keys EXPIRE 함수 구현
	 * 
	 * @param String key, long liveTime
	 * @return int 
	 * 1 if the timeout was set. 
	 * 0 if key does not exist or the timeout could not be set.
	 * @author 송원근
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public int EXPIRE(String key, long liveTime) throws ClassNotFoundException, IOException {
		if(EXISTS(key)){
			timeStamp = Calendar.getInstance().getTimeInMillis() / 1000;
			mmg.getEpList().add(new ExpireObject(MemoryManager.TYPE_KEYS, key, timeStamp+liveTime));
			return 1;
		}
		return 0;
	}
	
	/**
	 * Keys DEL 함수 구현
	 * 
	 * @param String key
	 * @return Object
	 * @author 송원근
	 */
	public Object DEL(String key) {
		if(EXISTS(key)){
			// Keys 에서 remove
			ManagedData md = mmg.getKeys().remove(key);
			// ArrayList 에서 remove
			mmg.getMdList().remove(md);
			mmg.del(md.getPos(), md.getBufferSize());
			return md;
		}

		return null; // fail
	}
	
	/**
	 * Keys DELBACKUP 함수 구현
	 * 
	 * @param String key
	 * @return Object
	 * @author 송원근
	 */
	public Object DELBACKUP(String key) {
		if(EXISTS(key)){
			// Keys 에서 remove
			ManagedData md = mmg.getKeys().remove(key);
			// Backup ArrayList 에서 remove
			mmg.getMdBackUpList().remove(md);
			mmg.delBackUp(md.getPos(), md.getBufferSize());
			return md;
		}

		return null; // fail

	}
	
	/**
	 * Keys PERSIST 함수 구현
	 * EXPIRE를 없애 주는 함수
	 * @param String key
	 * @return int 
	 *  1 if the timeout was removed. 
	 *  0 if key does not exist or does not have an associated timeout.
	 * @author 송원근
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public int PERSIST(String key) throws ClassNotFoundException, IOException {
		if(EXISTS(key)){
			for(int i=0; i < mmg.getEpList().size(); i++) {
				if( mmg.getEpList().get(i).getKey().equalsIgnoreCase(key) ) {
					mmg.getEpList().remove(i);
					return 1;
				}
			}
		}
		return 0;
	}
	
	/**
	 * Keys RENAMENX 함수 구현
	 * 
	 * @param String key, String newKey
	 * @return String 
	 *  1 if key was renamed to newkey. 
	 *  0 if newkey already exists.
	 * @author 송원근
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public int RENAMENX(String key, String  newKey) throws ClassNotFoundException, IOException {
		if(EXISTS(key)) {
			//mmg.getKeys() 에서 remove 하고 새로 넣어준다.
			ManagedData md = mmg.getKeys().remove(key);
			mmg.getKeys().put(newKey, md);
			int tempType = md.getType();
			int tempPos = md.getPos();
			
			for(int i=0; i < mmg.getMdList().size(); i++) {
				if( (tempType == mmg.getMdList().get(i).getType()) && 
						(tempPos == mmg.getMdList().get(i).getPos()) ) {
					//mmg.getMdList() 에서 새로운 key 값으로 설정
					mmg.getMdList().get(i).setKey(newKey);
				}
			}
			
			for(int i=0; i < mmg.getEpList().size(); i++) {
				if( (tempType == mmg.getEpList().get(i).getType()) && 
						(key == mmg.getEpList().get(i).getKey()) ) {
					//mmg.getMdList() 에서 새로운 key 값으로 설정
					mmg.getEpList().get(i).setKey(newKey);
				}
			}
			
			return 1;
		}
		return 0;
	}
	
	/**
	 * Keys SET 함수 구현
	 * 
	 * @param String key, Object value
	 * @return int 
	 * 1 if key and value insert success.
	 * 0 if key and value insert fail.
	 * @author 송원근
	 */
	public int SET(String key, Object value) throws IOException {
		byte[] objectBytes = (byte[])value;

		if(!EXISTS(key)) {
			if (mmg.getCurPosition() >= mmg.position()) {
				// 데이터를 넣은 후 바뀐 포지션을 보관하고 있는다.
				// 앞으로 바꿀 방법은 mmg.position과 현재 포지션을 비교하여 같지 않을 시에 mmg.positon
				// 으로 변경 후에 값을 넣으면 계속해서 뒤로 들어 가는 방향으로 생각하고 있음.
				
				mmg.put(objectBytes);
				
				ManagedData md = new ManagedData(MemoryManager.TYPE_KEYS, objectBytes.length,
						mmg.getCurPosition(), key);
				
				// ArrayList 에 ManagedData클래스를 넣는다.
				mmg.getMdList().add(md); // type,bufferSize,pos
				mmg.getKeys().put(key, md);
				mmg.setCurPosition(mmg.position());
				
				MessageManager.getInstance().insertMessage("SET key :"+ key);
				MessageManager.getInstance().insertMessage(md.toString());
				
				return 1;
			}
		}else {
			DEL(key);
			SET(key, value);
		}

		return 0;
	}
	
	/**
	 * Keys SETBACKUP 함수 구현
	 * 
	 * @param String key, Object value
	 * @return int 
	 * 1 if key and value insert success.
	 * 0 if key and value insert fail.
	 * @author 송원근
	 */
	public int SETBACKUP(String key, Object value) throws IOException {
		byte[] objectBytes = (byte[])value;

		if(!EXISTS(key)) {
			if (mmg.getCurPosition() >= mmg.position()) {
				MessageManager.getInstance().insertMessage("Before put position : "+ mmg.position());
				mmg.putBackUp(objectBytes);		//putBackUp() 메소드로 넣음
//				System.out.println("curPosition : "+ mmg.getCurPosition());
//				System.out.println("After put position : "+ mmg.position());
				ManagedData md = new ManagedData(MemoryManager.TYPE_KEYS, objectBytes.length,
						mmg.getCurPosition(), key);
				
				MessageManager.getInstance().insertMessage("curPosition : "+ mmg.getCurPosition());
				MessageManager.getInstance().insertMessage("After put position : "+ mmg.position());
				
				// BackUp ArrayList 에 ManagedData클래스를 넣는다.
				mmg.getMdBackUpList().add(md); // type,bufferSize,pos
				mmg.getKeys().put(key, md);
				mmg.setCurPosition(mmg.position());
				
				MessageManager.getInstance().insertMessage("SETBACKUP key :"+ key);
				MessageManager.getInstance().insertMessage(md.toString());
				
				return 1;
			}
		}else {
			DELBACKUP(key);
			SETBACKUP(key, value);
		}

		return 0;
	}
	
	/**
	 * Keys GET 함수 구현
	 * 
	 * @param String key
	 * @return Object
	 * @author 송원근
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public Object GET(String key) throws IOException,
			ClassNotFoundException {

		if(EXISTS(key)){
			ManagedData md = mmg.getKeys().get(key);
			
			int tempCount = md.getUseCount()+1;
			md.setUseCount(tempCount);
			
			MessageManager.getInstance().insertMessage("GET key :"+ key);
			MessageManager.getInstance().insertMessage(md.toString());
			return md;
		}
		MessageManager.getInstance().insertMessage("GET fail... "+ key+ " does not exist");
		return null; // fail
	}
	
	class ExpireManagerThread extends Thread {
		public void run() {
			MessageManager.getInstance().insertMessage("ExpireManagerThread start...");
			
			while(true) {
				long time = Calendar.getInstance().getTimeInMillis() / 1000;
				for(int i=0; i < mmg.getEpList().size(); i++) {
					if( time >=  mmg.getEpList().get(i).getExpireTime() ) {
						//mmg.getKeys() 와 mmg.getMdList() 에서 remove
						mmg.getMdList().remove(DEL(mmg.getEpList().get(i).getKey()));
						//mmg.getEpList() 에서 remove
						mmg.getEpList().remove(i);
						
						//test-----------
//						MessageManager.getInstance().insertMessage("remove success");
//						MessageManager.getInstance().insertMessage("key1 exists : "+ EXISTS("key1"));
//						MessageManager.getInstance().insertMessage("key2 exists : "+ EXISTS("key2"));
//						MessageManager.getInstance().insertMessage("key3 exists : "+ EXISTS("key3"));
//						MessageManager.getInstance().insertMessage("key4 exists : "+ EXISTS("key4"));
//						MessageManager.getInstance().insertMessage("key5 exists : "+ EXISTS("key5"));
//						MessageManager.getInstance().insertMessage("key6 exists : "+ EXISTS("key6"));
						//-------------
					}
				}
				
				try {
					Thread.sleep (1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
//			MessageManager.getInstance().insertMessage("ExpireManagerThread end...");
		}
		
	}
	
}
