package org.secmem.greenland.datastructure;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import org.secmem.greenland.nio.ManagedData;
import org.secmem.greenland.nio.MemoryManager;

public class HashesProcess {
	private MemoryManager mmg;
	
	public HashesProcess(MemoryManager mmg) {
		this.mmg = mmg;
	}
	
	/**
	 * Hashes HDEL 함수 구현
	 * 
	 * @param String key, String subKey
	 * @return int
	 * Removes the specified fields from the hash stored at key. 
	 * Specified fields that do not exist within this hash are ignored. 
	 * If key does not exist, it is treated as an empty hash and this command returns 0.
	 * @author 송원근
	 */
	public Object HDEL(String key, String subKey) {
		if(HEXISTS(key, subKey)) {
			ManagedData md = (ManagedData)mmg.getHashes().get(key).get(subKey);
			//hashes의 value(HashMap) 에서 remove
			mmg.getHashes().get(key).remove(subKey);
			//remove 해서 0이 된다면 hashes에서 지운다.
			if(mmg.getHashes().get(key).size() == 0) {
				mmg.getHashes().remove(key);
			}
			//ArrayList 에서 remove
			mmg.getMdList().remove(md);
			mmg.del(md.getPos(), md.getBufferSize());
			return md;
		}
			
		return null;
	}
	
	/**
	 * Hashes HEXISTS 함수 구현
	 * 
	 * @param String key, String subKey
	 * @return int
	 * 1 if the hash contains field.
	 * 0 if the hash does not contain field, or key does not exist.
	 * @author 송원근
	 */
	public boolean HEXISTS(String key, String subKey) {
		if(mmg.getHashes().containsKey(key)) {
			if(mmg.getHashes().get(key).containsKey(subKey))
				return true;
		}
		return false;
	}
	
	/**
	 * Hashes HGET 함수 구현
	 * 
	 * @param String key, String subKey
	 * @return Object
	 * Returns the value associated with field in the hash stored at key.
	 * @author 송원근
	 */
	public Object HGET(String key, String subKey) throws IOException,
			ClassNotFoundException {
		if(HEXISTS(key, subKey)) {
			ManagedData md = mmg.getHashes().get(key).get(subKey);
			int tempCount = md.getUseCount()+1;
			md.setUseCount(tempCount);
			return md;
		}
			
		return null;
	}
	
	/**
	 * Hashes HSET 함수 구현
	 * 
	 * @param String key, String subKey, Object value
	 * @return int
	 * 1 if field is a new field in the hash and value was set.
	 * 0 if field already exists in the hash and the value was updated.
	 * @author 송원근
	 * @throws IOException 
	 */
	public int HSET(String key, String subKey, Object value) throws IOException {
		byte[] objectBytes = (byte[])value;
		
		if(!HEXISTS(key, subKey)){
			if (mmg.getCurPosition() >= mmg.position()) {
				mmg.put(objectBytes);
				
				ManagedData md = new ManagedData(mmg.TYPE_HASHES, objectBytes.length,
						mmg.getCurPosition(), key, subKey);
				// ArrayList 에 ManagedData클래스를 넣는다.
				mmg.getMdList().add(md);
			
				//parentKey 가 존재한다면
				if(mmg.getHashes().containsKey(key)) {
					mmg.getHashes().get(key).put(subKey, md);
				}else {
					HashMap<String, ManagedData> tempHash = new HashMap<String, ManagedData>();
					tempHash.put(subKey, md);
					mmg.getHashes().put(key, tempHash);
				}
				
				mmg.setCurPosition(mmg.position());
				
				System.out.println("HSET key :"+ key+ " subKey :"+ subKey);
				System.out.println(md.toString());
				
				return 1;
				
			} 
		} else {
			HDEL(key, subKey);
			HSET(key, subKey, value);
		}
		
		return 0;
	}
	
	/**
	 * Hashes HGETALL 함수 구현
	 * 
	 * @param String key
	 * @return HashMap<String, Object>
	 * list of fields and their values stored in the hash, or an empty list when key does not exist.
	 * @author 송원근
	 */
	public HashMap<String, ManagedData> HGETALL(String key) {
		if(mmg.getHashes().containsKey(key)) {
			return mmg.getHashes().get(key);
		}
		return null;
	}
	
	/**
	 * Hashes HKEYS 함수 구현
	 * 
	 * @param String key, String subKey
	 * @return String []
	 * list of fields in the hash, or an empty list when key does not exist.
	 * @author 송원근
	 */
	public String [] HKEYS(String key) {
		if(mmg.getHashes().containsKey(key)) {
			Iterator<String> it = mmg.getHashes().get(key).keySet().iterator();
			String[] retStringArray = new String[mmg.getHashes().get(key).size()];
			
			int idx=0;
			String tempSubKey;
			while(it.hasNext()) {
				tempSubKey = it.next();
				retStringArray[idx++] = tempSubKey;
			}
			return retStringArray;
		}
		return null;
	}
	
	/**
	 * Hashes HLEN 함수 구현
	 * 
	 * @param String key
	 * @return int
	 * number of fields in the hash, or 0 when key does not exist.
	 * @author 송원근
	 */
	public int HLEN(String key) {
		return mmg.getHashes().get(key).size();
	}
	
//	/**
//	 * Hashes HMGET 함수 구현
//	 * 
//	 * @param String key, String [] subKeys
//	 * @return ArrayList<HashMap<String, Object>>
//	 *  list of values associated with the given fields, in the same order as they are requested.
//	 * @author 송원근
//	 */
//	public ArrayList<HashMap<String, Object>> HMGET(String key, String [] subKeys) {
//		return null;
//	}
//	
//	/**
//	 * Hashes HMSET 함수 구현
//	 * 
//	 * @param String key, HashMap<String, Object> data
//	 * @return int
//	 *  1 success, 0 fail.
//	 * @author 송원근
//	 */
//	public int HMSET(String key, HashMap<String, Object> data) {
//		return 0;
//	}
//	
//	/**
//	 * Hashes HSETNX 함수 구현
//	 * 
//	 * @param String key, String subKeys
//	 * @return int
//	 * 1 if field is a new field in the hash and value was set.
//	 * 0 if field already exists in the hash and no operation was performed.
//	 * @author 송원근
//	 */
//	public int HSETNX(String key, String subKeys) {
//		return 0;
//	}

}
