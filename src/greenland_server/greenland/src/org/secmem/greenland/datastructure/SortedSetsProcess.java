package org.secmem.greenland.datastructure;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.secmem.greenland.nio.ManagedData;
import org.secmem.greenland.nio.MemoryManager;

public class SortedSetsProcess {
	private MemoryManager mmg;
	
	public SortedSetsProcess(MemoryManager mmg) {
		this.mmg = mmg;
	}
	
	
	/**
	 * Sorted Sets ZEXISTSKEY 함수 구현
	 * 
	 * @param String key
	 * @return boolean
	 * 관리하는 해쉬맵에서 key값이 있는지만 판별
	 * @author 송원근
	 */
	public boolean ZEXISTSKEY(String key) {
		return mmg.getSortedSet().containsKey(key);
	}
	
	/**
	 * Sorted Sets ZADD 함수 구현
	 * 
	 * @param String key, String subKey, Object value
	 * @return int
	 * 
	 * @author 송원근
	 */
	public int ZADD(String key, String subKey, int value) {
		ManagedData md = new ManagedData(MemoryManager.TYPE_SORETEDSETS, key, subKey, value);
		
		if(ZEXISTSKEY(key)) {
			Iterator<ManagedData> it = mmg.getSortedSet().get(key).iterator();
			while(it.hasNext()) {
				ManagedData tempMd = it.next();
				if(tempMd.getSubKey().equalsIgnoreCase(subKey) ) {
					//key,subkey 가 중복이면 벨류를 업데이트해준다.
					ZREM(key, subKey);
					ZADD(key, subKey, value);
					//
					System.out.println("key : "+ key+ "  subKey : "+ subKey+ "  value "+ value);
					return 1;
				}
			}
			//중복이 없을 시에는 
			mmg.getSortedSet().get(key).add(md);		//sortedSet add
			mmg.getMdList().add(md);							//mdList add
			return 1;
		} else {
			SortedSet<ManagedData> s = new TreeSet<ManagedData>();
			s.add(md);
			mmg.getSortedSet().put(key, s);
			mmg.getMdList().add(md);
			return 1;
		}
	}

	/**
	 * Sorted Sets ZCARD 함수 구현
	 * 
	 * @param String key
	 * @return int
	 * 
	 * @author 송원근
	 */
	public int ZCARD(String key) {
		return mmg.getSortedSet().get(key).size();
	}
	
	/**
	 * Sorted Sets ZRANGE 함수 구현
	 * 
	 * @param String key, int start,  int stop
	 * @return String[]
	 * 
	 * @author 송원근
	 */
	public String [] ZRANGE(String key, int start, int stop) {
		if(ZEXISTSKEY(key)) {
			int tempSize = mmg.getSortedSet().get(key).size();
			int startIdx,stopIdx;
			if(start < 0) {
				startIdx = tempSize + start;
			} else {
				startIdx = start;
			}
			
			if(stop < 0) {
				stopIdx = tempSize + stop;
			}else {
				stopIdx = stop;
			}
			System.out.println(startIdx);
			System.out.println(stopIdx);
			String[] retStringArray = new String[stopIdx-startIdx+1];
			
			if(startIdx < 0 || stopIdx < 0) {
				return null;
			}
			
			Iterator<ManagedData> it = mmg.getSortedSet().get(key).iterator();
			ManagedData md;
			
			int temp=0;
			int strIdx=0;
			while(it.hasNext()) {
				md = it.next();
				
				if( (startIdx <= temp) && (temp <= stopIdx) ) {
					System.out.println(md.getSubKey());
					retStringArray[strIdx++] = md.getSubKey();
				}
				
				temp++;
			}
			
			return retStringArray;
			
		}
		return null;
	}
	
	/**
	 * Sorted Sets ZRANGEBYSCORE 함수 구현
	 * 
	 * @param String key, int top,  int bottom
	 * @return ArrayList<Object>
	 * 
	 * @author 송원근
	 */
	public ArrayList<Object> ZRANGEBYSCORE(String key, int min, int max) {
		if(ZEXISTSKEY(key)) {
			ArrayList<Object> retArrayList = new ArrayList<Object>();
			Iterator<ManagedData> it = mmg.getSortedSet().get(key).iterator();
			ManagedData md;
			while(it.hasNext()) {
				md = it.next();
				if((md.getSortedSetScore() >= min) && (md.getSortedSetScore() <= max) ) {
					retArrayList.add(md);
					//test
					System.out.println(md.getSubKey());
				}
			}
			return retArrayList;
			
		}
		return null;
	}
	
	/**
	 * Sorted Sets ZRANGEWITHSCORE 함수 구현
	 * 
	 * @param String key, int start, int stop
	 * @return Object
	 *  return value,score
	 * @author 송원근
	 */
	public ArrayList<Object> ZRANGEWITHSCORE(String key, int start, int stop) {
		if(ZEXISTSKEY(key)) {
			int tempSize = mmg.getSortedSet().get(key).size();
			int startIdx,stopIdx;
			if(start < 0) {
				startIdx = tempSize + start;
			} else {
				startIdx = start;
			}
			
			if(stop < 0) {
				stopIdx = tempSize + stop;
			}else {
				stopIdx = stop;
			}
			System.out.println(startIdx);
			System.out.println(stopIdx);
			ArrayList<Object> retArrayList = new ArrayList<Object>();
			
			if(startIdx < 0 || stopIdx < 0) {
				return null;
			}
			
			Iterator<ManagedData> it = mmg.getSortedSet().get(key).iterator();
			ManagedData md;
			
			int temp=0;
			int strIdx=0;
			while(it.hasNext()) {
				md = it.next();
				
				if( (startIdx >= temp) || (temp <= stopIdx) ) {
					System.out.println(md.getSubKey());
					System.out.println(md.getSortedSetScore());
					retArrayList.add(md);
				}
				
				temp++;
			}
			
			return retArrayList;
			
		}
		return null;
	}

	/**
	 * Sorted Sets ZRANK 함수 구현
	 * 
	 * @param int value, String Key
	 * @return Object
	 * @author 송원근
	 */
	public int ZRANK(String key, String subKey) {
		if(ZEXISTSKEY(key)) {
			Iterator<ManagedData> it = mmg.getSortedSet().get(key).iterator();
			ManagedData md;
			int retRank=0;
			while(it.hasNext()) {
				retRank++;
				md = it.next();
				if(md.getSubKey().equalsIgnoreCase(subKey)){
					return retRank;
				}
			}
		}
		return 0;
	}
	
	
	/**
	 * Sorted Sets ZREM 함수 구현
	 * 
	 * @param int value, String Key
	 * @return Object
	 * Removes the specified members from the sorted set stored at key. 
	 * Non existing members are ignored.
	 * An error is returned when key exists and does not hold a sorted set.
	 * The number of members removed from the sorted set, not including non existing members.
	 * @author 송원근
	 */
	public Object ZREM(String key, String subKey) {
		if(ZEXISTSKEY(key)) {
			ManagedData md;
			Iterator<ManagedData> it = mmg.getSortedSet().get(key).iterator();
			while(it.hasNext()) {
				md = it.next();
				if(md.getSubKey().equalsIgnoreCase(subKey)) {
					mmg.getSortedSet().get(key).remove(md);		//sorted Set 에서 remove
					mmg.getMdList().remove(md);						//mdList 에서 remove
					//지우고 나서 해당 키값에 데이터가 없다면 키도 지워준다.
					if(mmg.getSortedSet().get(key).size() == 0) {
						mmg.getSortedSet().remove(key);
					}
					return md;
				}
			}
		}
		return null;
	}
	
	/*
	public int ZREVRANK(String key, String subKey) {
		return 0;
	}
	
	public String [] ZREVRANGE(String key, int top, int bottom) {
		return null;
	}
	
	public ArrayList<HashMap<String, Object>> ZREVRANGEBYSCORE(String key, int top, int bottom) { 
		return null;
	}
	
	public ArrayList<HashMap<String, Object>> ZREMRANGEBYRANK(String key, String subKey, int top, int bottom) {
		return null;
	}
	
	public ArrayList<HashMap<String, Object>> ZREMREVRANGEBYRANK(String key, String subKey, int top, int bottom) {
		return null;
	}
	*/
}
