package org.secmem.greenland.config;

import java.io.File;
import java.io.Serializable;
import java.net.InetAddress;
import java.util.HashMap;

public class ServerInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private InetAddress serverAddr;
	private String serverAlias;
	private int serverIndex;
	private int backupServerIndex;
	private int memoryUsage;
	private int memoryCapacity;
	private String serverUUID;
	private boolean isAlive;
	
	private int primaryKeyCount;
	private int backupKeyCount;
	
	private HashMap<String, Object> infoTable;
	
	private long primaryMemoryUsage;
	public void setPirmaryMemoryUsage(long usage){
		primaryMemoryUsage = usage;
	}
	public long getPrimaryMemoryUsage(){
		return primaryMemoryUsage;
	}
	
	private long backupMemoryUsage;
	public void setBackupMemoryUsage(long usage){
		backupMemoryUsage = usage;
	}
	public long getBackupMemoryUsage(){
		return backupMemoryUsage;
	}
	
	public int getTotalKeyCount(){
		return primaryKeyCount + backupKeyCount;
	}

	
	public void setServerUUID(String uuid){
		serverUUID = uuid;
	}
	public String getServerUUID(){
		return serverUUID;
	}
	
	public long getMemoryUsage(){
		return primaryMemoryUsage+backupMemoryUsage;
	}
	
	public ServerInfo(String alias, InetAddress addr, int size, int index, int backupIndex){
		super();
		serverAlias = alias;
		serverAddr = addr;
		memoryCapacity = size;		
		serverIndex = index;
		backupServerIndex = backupIndex;
		
		infoTable = new HashMap<String, Object>();
		infoTable.put("core", Runtime.getRuntime().availableProcessors());
		infoTable.put("freememory", Runtime.getRuntime().freeMemory());
		infoTable.put("maxmemory", Runtime.getRuntime().maxMemory());
		infoTable.put("availablejvmmemory", Runtime.getRuntime().totalMemory());
		
		File[] roots  = File.listRoots();
		
		long freeSpace = 0l;
		long totalSpace = 0l;
		long usableSpace = 0l;
		
		for(File root : roots) {
			freeSpace += root.getFreeSpace();
			totalSpace += root.getTotalSpace();
			usableSpace += root.getUsableSpace();
		}
		
		infoTable.put("freespace", freeSpace);
		infoTable.put("totalspace", totalSpace);
		infoTable.put("usablespace", usableSpace);
		
		infoTable.put("javaversion", System.getProperty("java.version"));
		infoTable.put("javavendor", System.getProperty("java.vendor"));
		infoTable.put("javahome", System.getProperty("java.home"));
		infoTable.put("osname", System.getProperty("os.name"));
		infoTable.put("osarch", System.getProperty("os.arch"));
		infoTable.put("osversion", System.getProperty("os.version"));
		infoTable.put("username", System.getProperty("user.name"));
		infoTable.put("userhome", System.getProperty("user.home"));
		infoTable.put("userdir", System.getProperty("user.dir"));
	}
	
	public int sendKeepAlive(){
		//RMI send Keep-Alive packet
		return 0;
	}
	
	public InetAddress getServerAddr() {
		return serverAddr;
	}
	public String getAlias() {
		return serverAlias;
	}
	public int getSize() {
		return memoryCapacity;
	}
	
	public HashMap<String, Object> getSystemInfo(){
		return infoTable;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("serverIndex=");
		sb.append(serverIndex);
		sb.append(",");
		sb.append("backupIndex=");
		sb.append(backupServerIndex);
		sb.append(",");
		sb.append("alias=");
		sb.append(serverAlias);
		sb.append(",");
		sb.append("addr=");
		sb.append(serverAddr.getHostAddress());
		sb.append(",");
		sb.append("memoryUsage=");
		sb.append(memoryUsage);
		sb.append(",");
		sb.append("memoryCapacity=");
		sb.append(memoryCapacity);
		sb.append(",");
		sb.append("memoryUsingRate=");
		sb.append((memoryUsage/memoryCapacity)*100);
		sb.append("%");
		return sb.toString();
	}
	public int getServerIndex() {
		return serverIndex;
	}
	public void setServerIndex(int serverIndex) {
		this.serverIndex = serverIndex;
	}
	public int getBackupServerIndex() {
		return backupServerIndex;
	}
	public void setBackupServerIndex(int backupServerIndex) {
		this.backupServerIndex = backupServerIndex;
	}
	public boolean isAlive() {
		return isAlive;
	}
	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}
	public int getPrimaryKeyCount() {
		return primaryKeyCount;
	}
	public void setPrimaryKeyCount(int primaryKeyCount) {
		this.primaryKeyCount = primaryKeyCount;
	}
	public int getBackupKeyCount() {
		return backupKeyCount;
	}
	public void setBackupKeyCount(int backupKeyCount) {
		this.backupKeyCount = backupKeyCount;
	}
	
	

	
}

