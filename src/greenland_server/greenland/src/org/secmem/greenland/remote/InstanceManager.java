package org.secmem.greenland.remote;

import java.net.InetAddress;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;

public interface InstanceManager extends Remote {
	public int addInstance(InstanceConnection conn) throws RemoteException;
	public int removeInstance(int index) throws RemoteException;
	public int getInstanceCount() throws RemoteException;
	public InstanceConnection findInstance(InetAddress addr) throws RemoteException;
	public int checkKeepAlive() throws RemoteException;
	public InstanceConnection getLocalInstance() throws RemoteException;
	public InstanceConnection getInstance(int index) throws RemoteException;
	public HashMap<Integer, InstanceConnection> getInstanceAll() throws RemoteException;
	
	public Boolean StartLocking(int index) throws RemoteException;
	public Boolean StartLockingAll() throws RemoteException;
	public Boolean ReleaseLock(int index) throws RemoteException;
	public Boolean ReleaseLockingAll() throws RemoteException;
	
	public Boolean RemappingServerKey() throws RemoteException;
}
