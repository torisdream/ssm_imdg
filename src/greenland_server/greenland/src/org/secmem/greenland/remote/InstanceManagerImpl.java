package org.secmem.greenland.remote;

import java.net.InetAddress;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;

import org.secmem.greenland.config.ServerInfo;
import org.secmem.greenland.message.MessageManager;

public class InstanceManagerImpl extends UnicastRemoteObject implements
		InstanceManager {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	HashMap<Integer,InstanceConnection> instanceList;
	InstanceConnection localhostConn;
	
	public static final int KEEPALIVE_OK = -1;
	
	public InstanceConnection getLocalInstance() throws RemoteException{
		return localhostConn;
	}
	
	private InetAddress hostAddr;
	
	public InstanceManagerImpl() throws RemoteException{
		super();
		
		instanceList = new HashMap<Integer, InstanceConnection>();
	}
	
	public InstanceManagerImpl(InetAddress host) throws RemoteException{
		super();
		
		hostAddr = host;
		instanceList = new HashMap<Integer, InstanceConnection>();
	}	
	
	

	@Override
	public int addInstance(InstanceConnection conn) throws RemoteException {
		ServerInfo s = conn.getServerInfo();
		if(hostAddr.getHostAddress().equals(s.getServerAddr().getHostAddress())){
			localhostConn = conn;
		}
		instanceList.put(conn.getServerInfo().getServerIndex(), conn);
	
		return 0;
	}

	@Override
	public int removeInstance(int index) throws RemoteException {
		instanceList.remove(index);
		
		return 1;
	}

	@Override
	public int checkKeepAlive() throws RemoteException {
		int serverCount = 0;
		
		for(int i:instanceList.keySet()){
			try{
				if(instanceList.get(i).checkAlive() == 0){
					serverCount++;
				}
			}catch(Exception e){
				MessageManager.getInstance().insertMessage(i+"th server Keep-Alive check error: "+e.getMessage());
				return i;
			}
			
		}
		
		return KEEPALIVE_OK;
	}

	@Override
	public InstanceConnection findInstance(InetAddress addr) throws RemoteException {
		for(int i=0;i<instanceList.size();i++){
			if(addr.getHostAddress().equals(instanceList.get(i).getServerInfo().getServerAddr().getHostAddress())){
				return instanceList.get(i);
			}
		}
		return null;
	}

	@Override
	public InstanceConnection getInstance(int index) throws RemoteException {
		return instanceList.get(index);
	}

	@Override
	public Boolean StartLocking(int index) throws RemoteException {
		if(localhostConn.getServerInfo().getServerIndex() != 0){
			return false;
		}
		InstanceConnection targetConn = getInstance(index);
		return targetConn.startLocking();
	}

	@Override
	public Boolean StartLockingAll() throws RemoteException {
		if(localhostConn.getServerInfo().getServerIndex() != 0){
			return false;
		}
		
		Boolean ret = true;
		
		for(int i : instanceList.keySet()){
			if(!StartLocking(i)){
				ret = false;
			}
		}
		return ret;
	}

	@Override
	public Boolean ReleaseLock(int index) throws RemoteException {
		if(localhostConn.getServerInfo().getServerIndex() != 0){
			return false;
		}
		InstanceConnection targetConn = getInstance(index);
		return targetConn.releaseLocking();
	}

	@Override
	public Boolean ReleaseLockingAll() throws RemoteException {
		if(localhostConn.getServerInfo().getServerIndex() != 0){
			return false;
		}
		
		Boolean ret = true;
		
		for(int i : instanceList.keySet()){
			if(!ReleaseLock(i)){
				ret = false;
			}
		}
		return ret;
	}

	@Override
	public int getInstanceCount() throws RemoteException {
		return instanceList.size();
	}

	@Override
	public HashMap<Integer, InstanceConnection> getInstanceAll()
			throws RemoteException {
		return instanceList;
	}

	@Override
	public Boolean RemappingServerKey() throws RemoteException {
		HashMap<Integer, InstanceConnection> newList = new HashMap<Integer, InstanceConnection>();
		
		for(int key : instanceList.keySet()){
			InstanceConnection conn = instanceList.get(key);
			
			newList.put(conn.getServerInfo().getServerIndex(), conn);
		}
		
		instanceList = newList;
		
		return true;
	}

}
