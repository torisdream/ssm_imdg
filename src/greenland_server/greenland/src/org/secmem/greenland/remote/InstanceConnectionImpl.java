package org.secmem.greenland.remote;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;

import org.secmem.greenland.config.ServerInfo;
import org.secmem.greenland.exception.ServerLockingException;
import org.secmem.greenland.message.MessageManager;
import org.secmem.greenland.nio.ManagedData;
import org.secmem.greenland.nio.MemoryManager;
import org.secmem.greenland.nio.PullDirectBuffer;
import org.secmem.greenland.statemachine.GreenlandStateMachine;

public class InstanceConnectionImpl extends UnicastRemoteObject implements
		InstanceConnection {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ServerInfo serverInfo;
	private CommandBuilder commandBuilder;
	private MessageManager messageManager;
	
	public InstanceConnectionImpl() throws RemoteException{
		super();
		serverInfo = null;
	}

	@Override
	public int checkAlive() throws RemoteException {
		return 0;
	}

	@Override
	public Boolean findData(String key) throws RemoteException, ServerLockingException, ServerNotActiveException {
		return GreenlandStateMachine.getInstance().getInstanceManager().getLocalInstance().getCommandBuilder().EXISTS(key);
		
	}
	
	@Override
	public Boolean findData(String parentKey, String key) throws RemoteException, ServerLockingException, ServerNotActiveException {
		GreenlandStateMachine.getInstance().getInstanceManager().getLocalInstance().getCommandBuilder().HEXISTS(parentKey, key);
		return false;
	}
	
	@Override
	public CommandBuilder getCommandBuilder() throws RemoteException, ServerLockingException, ServerNotActiveException {
		if(GreenlandStateMachine.getInstance().getIsLocking() && getClientHost() != GreenlandStateMachine.getInstance().getInstanceManager().getInstance(0).getServerInfo().getServerAddr().getHostAddress()){
			throw new ServerLockingException(serverInfo.getServerAddr().getHostAddress());
		}
		return commandBuilder;
	};
	
	@Override
	public int setCommandBuilder(CommandBuilder builder) throws RemoteException {
		commandBuilder = builder;
		return 0;
	};
	

	@Override
	public int setServerInfo(ServerInfo info) throws RemoteException {
		serverInfo = info;
		return 0;
	}

	@Override
	public ServerInfo getServerInfo() throws RemoteException {
		return serverInfo;
	}

	@Override
	public int sendRawData(String key, Byte[] data) throws RemoteException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public MessageManager getMessageManager() throws RemoteException {
		return messageManager;
	}

	@Override
	public void setMessageManager(MessageManager message)
			throws RemoteException {
		messageManager = message;
		return ;
	}

	@Override
	public Boolean startLocking() throws RemoteException {
		GreenlandStateMachine.getInstance().setIsLocking(true);
		return GreenlandStateMachine.getInstance().getIsLocking();
	}

	@Override
	public Boolean releaseLocking() throws RemoteException {
		GreenlandStateMachine.getInstance().setIsLocking(false);
		return GreenlandStateMachine.getInstance().getIsLocking();
	}

	@Override
	public Boolean defragment() throws RemoteException {
		try{
			PullDirectBuffer pull = new PullDirectBuffer(GreenlandStateMachine.getInstance().getMemoryManager());
			pull.pullDirectBuffer();
			return true;	
		}catch(Exception e){
			messageManager.insertMessage(e.getMessage());
			return false;
		}
	}

	
	@Override
	public HashMap<String, Object> getDataWithSize(int type, long size, boolean removeData)
			throws RemoteException {
		
		HashMap<String, Object> ret = null;
		
		if(type == DATATYPE_PRIMARY){
			ret = GreenlandStateMachine.getInstance().getMemoryManager().readMemory(size,removeData);
		}else if(type == DATATYPE_BACKUP){
			ret = GreenlandStateMachine.getInstance().getMemoryManager().readBackupMemory(size);
		}
		
		return ret;
	}

	@Override
	public int putDataWithSize(int type, HashMap<String, Object> data)
			throws RemoteException {
		
		if(type == DATATYPE_PRIMARY){
			GreenlandStateMachine.getInstance().getMemoryManager().writeMemory(data);
		}else if(type == DATATYPE_BACKUP){
			GreenlandStateMachine.getInstance().getMemoryManager().writeBackUpMemory(data);
		}
		
//		ArrayList<ManagedData> mdList = (ArrayList<ManagedData>)data.get("mdList");
//		ArrayList<byte []> dataList = (ArrayList<byte []>)data.get("byteLinkedList");
//		
//		for(int i=0;i<mdList.size();i++){
//			ManagedData md = mdList.get(i);
//			
//			try {
//			
//				if(type == DATATYPE_PRIMARY){
//					switch(md.getType()){
//						case MemoryManager.TYPE_KEYS:
//						
//							commandBuilder.SET(md.getKey(), dataList.get(i));
//						break;
//						
//						case MemoryManager.TYPE_HASHES:
//							commandBuilder.HSET(md.getKey(), md.getSubKey(), dataList.get(i));
//						break;
//					}
//					
//				}else if(type == DATATYPE_BACKUP){
//					commandBuilder.SETBACKUP(md.getType(), md.getKey(), md.getSubKey(), dataList.get(i));
//				}
//				
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				continue;
//			} catch (ServerLockingException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				continue;
//			} catch (ServerNotActiveException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				continue;
//			}
//				
//				
//			
//		}
//		
		return 0;
	}

	@Override
	public Boolean clearBackupMemory() throws RemoteException {
		GreenlandStateMachine.getInstance().getMemoryManager().clearBackUpMdList();
		return true;
	}

	@Override
	public int setServerIndex(int type, int index) throws RemoteException {
		if(type == DATATYPE_PRIMARY){
			serverInfo.setServerIndex(index);
		}else if(type == DATATYPE_BACKUP){
			serverInfo.setBackupServerIndex(index);
		}
		
		return index;
	}

	@Override
	public void forceMemoryUpdate() throws RemoteException {
		ServerInfo s = GreenlandStateMachine.getInstance().getInstanceManager().getLocalInstance().getServerInfo();
		MemoryManager m = GreenlandStateMachine.getInstance().getMemoryManager();
		s.setPrimaryKeyCount(m.getMdList().size());
		s.setPirmaryMemoryUsage(m.getUsingMemorySize());
		s.setBackupKeyCount(m.getMdBackUpList().size());
		s.setBackupMemoryUsage(m.getUsingBackUpMemorySize());
		
	}
	
	

}
