package org.secmem.greenland.cluster;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.secmem.greenland.config.GreenlandConfig;
import org.secmem.greenland.config.ServerInfo;
import org.secmem.greenland.nio.ManagedData;
import org.secmem.greenland.remote.InstanceConnection;
import org.secmem.greenland.remote.InstanceManager;
import org.secmem.greenland.statemachine.GreenlandStateMachine;
import org.xml.sax.SAXException;

public class ServerModificationAction {
	
	private static boolean isWork;
	
	private static ServerModificationAction instance;
	private static boolean isMaster;
	
	public final static int SUCCESS = 0;
	public final static int FAILURE = 1;
	public final static int ERROR = 2;
	
	private final static int MERGINGTYPE_EXCEPT = 0;
	private final static int MERGINGTYPE_ADD = 1;
	private final static int MERGINGTYPE_STILL = 2;
	
	
	//buffer unit is MB(megabyte)
	private final static int MERGE_BUFEER_LIMIT = 1;
	
	
	
	private ServerModificationAction() throws SAXException, IOException, ParserConfigurationException{
		if(GreenlandConfig.getInstance().getLocalServerInfo().getServerIndex() != 0){
			isMaster = false;
			isWork = false;
			return ;
		}
		isMaster = true;
	}
	
	public static ServerModificationAction getInstance() throws SAXException, IOException, ParserConfigurationException{
		if(instance == null)
			instance = new ServerModificationAction();
		
		if(!isMaster)
			return null;
		
		return instance;
	}
	
	private void startWork(){
		try {
			GreenlandStateMachine.getInstance().getInstanceManager().StartLockingAll();
			isWork = true;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		
	}
	private void endWork(){
		try {
			GreenlandStateMachine.getInstance().getInstanceManager().ReleaseLockingAll();
			isWork = false;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
	}
	
	public boolean getWorkerState(){
		return isWork;
	}
	
	public boolean exceptInstance(int index) throws SAXException, IOException, ParserConfigurationException{
		List<ServerInfo> serverList = GreenlandConfig.getInstance().getServerList();
		
		InstanceManager im = GreenlandStateMachine.getInstance().getInstanceManager();
		HashMap<Integer, InstanceConnection> connList = im.getInstanceAll();
		connList.remove(index);
		
		for(ServerInfo info : serverList){
			if(info.getServerIndex() == index){
				int ret = MergingInstances(MERGINGTYPE_EXCEPT, index);
				if(ret == 0){
					return true;
				}else{
					return false;
				}
			}
		}
		
		return false;
	}
	
	private int MergingInstances(int mergingType, int value) throws RemoteException{
		startWork();
		
		InstanceManager im = GreenlandStateMachine.getInstance().getInstanceManager();
		
		HashMap<Integer, InstanceConnection> connList = im.getInstanceAll();
		
		int exceptTarget = -1;
		
		if(mergingType == MERGINGTYPE_EXCEPT){
			int connCount = connList.size();
			
			exceptTarget = value+1;
			if(exceptTarget >= connCount+1)
				exceptTarget -= connCount+1;
			
			if(connCount == 1){
				InstanceConnection conn = connList.get(0); 
				conn.getServerInfo().setServerIndex(0);
				conn.getServerInfo().setBackupServerIndex(0);
				long backupMemorySize = conn.getServerInfo().getBackupMemoryUsage();
				HashMap<String, Object> dataHash = conn.getDataWithSize(InstanceConnection.DATATYPE_BACKUP, backupMemorySize, true);
				conn.putDataWithSize(InstanceConnection.DATATYPE_PRIMARY, dataHash);
				conn.clearBackupMemory();
				conn.defragment();
				return SUCCESS;
			}
			
		}
		
		long [] usageArray  = new long[im.getInstanceCount()+1];
		long [] destUsageArray  = new long[im.getInstanceCount()+1];
		long [] capacityArray  = new long[im.getInstanceCount()+1];
		double [] capacityRatioArray = new double[im.getInstanceCount()+1];
		
		Integer [] keySets = (Integer [])connList.keySet().toArray(new Integer[]{});
		
		long minimumCapacity = 2147483648l;
		long totalUsage = 0l;
		double divisor = 0l;
		
		long [][] distMap = new long[im.getInstanceCount()+1][im.getInstanceCount()+1];
		
		for(int i=0;i<keySets.length;i++){
			usageArray[i] = connList.get(keySets[i]).getServerInfo().getPrimaryMemoryUsage();
			capacityArray[i] = connList.get(keySets[i]).getServerInfo().getSize()*1024*1024;
			if(minimumCapacity > capacityArray[i])
				minimumCapacity = capacityArray[i];
			
			totalUsage += usageArray[i];
		}
		
		if(exceptTarget != -1){
			totalUsage += connList.get(exceptTarget).getServerInfo().getBackupMemoryUsage();
		}
		
		for(int i=0;i<keySets.length;i++){
			capacityRatioArray[i] = capacityArray[i] / (minimumCapacity*1.0);
			divisor += capacityRatioArray[i];
		}
		
		for(int i=0;i<keySets.length;i++){
			destUsageArray[i] = (long) Math.ceil((totalUsage / divisor) * capacityRatioArray[i]);
		}
		
		for(int i=0;i<keySets.length;i++){
			if(usageArray[i] < destUsageArray[i])
				continue;
			long remainMemory = usageArray[i] - destUsageArray[i];
			
			for(int j=0;j<keySets.length;j++){
				if(i==j)
					continue;
				
				if(destUsageArray[j] > usageArray[j]){
					if(remainMemory > destUsageArray[j] - usageArray[j]){
						distMap[i][j] = destUsageArray[j] - usageArray[j];
						usageArray[j] += destUsageArray[j] - usageArray[j];
						remainMemory -= destUsageArray[j] - usageArray[j];
					}else{
						distMap[i][j] = remainMemory;
						usageArray[j] += remainMemory;
						remainMemory = 0;
						break;
					}
				}
			}
		}
		if(exceptTarget != -1){
			long remainMemory = connList.get(exceptTarget).getServerInfo().getBackupMemoryUsage();
			for(int i=0;i<keySets.length;i++){
				if(destUsageArray[i] > usageArray[i]){
					if(remainMemory > destUsageArray[i] - usageArray[i]){
						distMap[keySets.length][i] = destUsageArray[i] - usageArray[i];
						remainMemory -= destUsageArray[i] - usageArray[i];
						usageArray[i] += destUsageArray[i] - usageArray[i];
					}else{
						distMap[keySets.length][i] = remainMemory;
						usageArray[i] += remainMemory;
						remainMemory = 0;
						break;
					}
				}
			}
		}
		
		
		//debug
		for(int i=0;i<distMap.length;i++){
			for(int j=0;j<distMap[i].length;j++){
				System.out.print(distMap[i][j]+" ");
			}
			System.out.println();
		}
		
		//process...
		for(int i=0;i<keySets.length;i++){
			for(int j=0;j<keySets.length;j++){
				if(i == j)
					continue;
				
				if(distMap[i][j] != 0){
					long targetAmount = distMap[i][j];
					InstanceConnection srcConn = connList.get(keySets[i]);
					InstanceConnection destConn = connList.get(keySets[j]);
					
					while(targetAmount != 0){
						long targetSize = (targetAmount > MERGE_BUFEER_LIMIT*1024*1024 ? MERGE_BUFEER_LIMIT*1024*1024 : targetAmount);
						
						HashMap<String, Object> transData = srcConn.getDataWithSize(InstanceConnection.DATATYPE_PRIMARY, targetSize, true);
						destConn.putDataWithSize(InstanceConnection.DATATYPE_PRIMARY, transData);
						
						//HashMap<String, Object> transData = srcConn.getCommandBuilder().getDataWithSize(targetSize);
						//transData - mdList -> HashMap<String, List<ManagedData>>: managed data list
						//transData - dataList -> HashMap<String key, byte [] data>: data list
						//destConn.getCommandBuilder.setDataWithHash(transData);
						
						targetAmount -= targetSize;						
					}
				}
			}
		}
		
		if(exceptTarget != -1){
			ArrayList<InstanceConnection> connArray = new ArrayList<InstanceConnection>();
			
			for(int i=0;i<keySets.length;i++){
				connArray.add(connList.get(keySets[i]));
			}
			
			for(int i=0;i<keySets.length;i++){
				if(distMap[keySets.length][i] != 0){
					long targetAmount = distMap[keySets.length][i];
					InstanceConnection srcConn = connList.get(exceptTarget);
					InstanceConnection destConn = connArray.get(i);
					
					int upCount = 0;
					
					while(targetAmount > 0){
						
						if(targetAmount < 100 && upCount > 0)
							break;
						
						long targetSize = (targetAmount > MERGE_BUFEER_LIMIT*1024*1024 ? MERGE_BUFEER_LIMIT*1024*1024 : targetAmount);
						
						HashMap<String, Object> transData = srcConn.getDataWithSize(InstanceConnection.DATATYPE_BACKUP, targetSize, true);
						destConn.putDataWithSize(InstanceConnection.DATATYPE_PRIMARY, transData);
						
						//HashMap<String, Object> transData = srcConn.getCommandBuilder().getBackupDataWithSize(targetSize);
						//transData - mdList -> HashMap<String, List<ManagedData>>: managed data list
						//transData - dataList -> HashMap<String key, byte [] data>: data list
						//destConn.getCommandBuilder.setDataWithHash(transData);
						
						targetAmount -= (Long)transData.get("size");
						upCount++;
					}
				}
			}
			
			int tempIndex = 0;
			
			for(int i=0;i<keySets.length;i++){
				InstanceConnection conn = connList.get(keySets[i]);
				
				int primaryIndex = tempIndex;
				int backupIndex = primaryIndex+1;
				
				if(backupIndex >= keySets.length)
					backupIndex -= keySets.length;
				
				
				conn.setServerIndex(InstanceConnection.DATATYPE_PRIMARY, primaryIndex);
				conn.setServerIndex(InstanceConnection.DATATYPE_BACKUP, backupIndex);
				
				tempIndex++;
			}
		}
		
		im.RemappingServerKey();
		
		connList = im.getInstanceAll();
		
		for(int key:connList.keySet()){
			connList.get(key).clearBackupMemory();
			connList.get(key).defragment();
			connList.get(key).forceMemoryUpdate();

		}
		
		for(int key:connList.keySet()){
			long targetAmount = connList.get(key).getServerInfo().getPrimaryMemoryUsage();
			int upCount = 0;
			
			while(targetAmount > 0){
				
				if(targetAmount < 100 && upCount > 0)
					break;
				long targetSize = (targetAmount > MERGE_BUFEER_LIMIT*1024*1024 ? MERGE_BUFEER_LIMIT*1024*1024 : targetAmount);
				HashMap<String, Object> moveData = connList.get(key).getDataWithSize(InstanceConnection.DATATYPE_PRIMARY, targetSize, false);
				connList.get(connList.get(key).getServerInfo().getBackupServerIndex()).putDataWithSize(InstanceConnection.DATATYPE_BACKUP, moveData);
				
				targetAmount -= (Long)moveData.get("size");
				upCount++;
			}
		}
		
		endWork();
		return 0;
	}
	
}
