package connection;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.util.ArrayList;
import java.util.HashMap;

import org.secmem.greenland.config.ServerInfo;
import org.secmem.greenland.exception.ServerLockingException;
import org.secmem.greenland.message.MessageManager;
import org.secmem.greenland.nio.ManagedData;
import org.secmem.greenland.remote.InstanceConnection;

public class GreenlandConnection {
	private ArrayList<InstanceConnection> connectionList;
	private int serverCount;
	private ArrayList<String> serverAddrList;
	private MessageManager messageManager;
	
	public GreenlandConnection(MessageManager manager, int serverCount, String [] serverAddrs){
		messageManager = manager;
		
		if(serverCount < 1){
			messageManager.insertMessage("input server count more than 1");
			return ;
		}
		
		if(serverCount != serverAddrs.length){
			messageManager.insertMessage("server ipaddress count not matched with server count");
		}
		
		this.serverCount = serverCount;
		
		connectionList =new ArrayList<InstanceConnection>();
		serverAddrList = new ArrayList<String>();
		
		for(String addr : serverAddrs){
			serverAddrList.add(addr);
		}
	}
	
	public Boolean openConnection(){
		for(String addr : serverAddrList){
			try {
				InstanceConnection conn = (InstanceConnection)Naming.lookup("//"+addr+"/GreenlandInstanceConnection");
				messageManager.insertMessage(conn.getServerInfo().toString());
				connectionList.add(conn);
			} catch (MalformedURLException e) {
				
				e.printStackTrace();
				continue;
				
			} catch (RemoteException e) {
				
				e.printStackTrace();
				continue;
				
			} catch (NotBoundException e) {
				
				e.printStackTrace();
				continue;
			}
		}
		
		return true;
	}
	
	public void getServerInfoAll(){
		for(int i=0;i<serverCount;i++){
			getServerInfo(i);
		}
	}
	
	public void getServerInfo(int index){
		InstanceConnection conn = connectionList.get(index);
		
		try {
			ServerInfo si = conn.getServerInfo();
			HashMap<String, Object> info = si.getSystemInfo();
			messageManager.insertMessage("get server "+si.getAlias()+" system info");
			for(String k : info.keySet()){
				messageManager.insertMessage(k+" ::  "+info.get(k).toString());
			}
		} catch (RemoteException e) {
			
			e.printStackTrace();
		}
		
	}
	
	private InstanceConnection getMinimumUsageServer(){
		double minimumUsage = 1;
		InstanceConnection ret = null;
		
		for(InstanceConnection c : connectionList){
			double ratio = 1;
			try {
				long usage = c.getServerInfo().getPrimaryMemoryUsage();	// getMemoryUsage
				long capacity = c.getServerInfo().getSize()*1024*1024;
				
				ratio = usage/(capacity*1.0);
				
				messageManager.insertMessage(c.getServerInfo().getAlias() + " - "  + usage + " / " + capacity + " : " + ratio);
			} catch (RemoteException e) {
				
				e.printStackTrace();
			}
			
			if(ratio < minimumUsage){
				minimumUsage = ratio;
				ret = c;
			}
		}
		return ret;
	}
	
	private InstanceConnection getConnectionContainsKey(String key){
		InstanceConnection ret = null;
		
		for(InstanceConnection c : connectionList){
			try{
				if(c.findData(key)){
					ret = c;
				}
			} catch(RemoteException e){
				
				e.printStackTrace();
				
			} catch (ServerLockingException e) {
				
				e.printStackTrace();
				
			} catch (ServerNotActiveException e) {
				
				e.printStackTrace();
			} 
		}
		
		return ret;
	}
	
	// Keys ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public int SET(String key, Object value){
		byte[] ct = null;
		
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ObjectOutputStream os;
			os = new ObjectOutputStream(out);
			os.writeObject(value);
			ct = out.toByteArray();
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}

		int result = -1;
		
		if(ct != null){
			try {
				InstanceConnection conn = getConnectionContainsKey(key);//null;
				if(conn != null){
					result = conn.getCommandBuilder().SET(key, ct); // <- conn.getCommandBuilder().SET(key, ct);
				}else{
					conn = getMinimumUsageServer();
					result = conn.getCommandBuilder().SET(key, ct);
				}
				messageManager.insertMessage("Target server: "+conn.getServerInfo().getServerAddr().getHostAddress()+" / key: "+key);
			} catch (RemoteException e) {
				
				e.printStackTrace();
				
			} catch (IOException e) {
				
				e.printStackTrace();
				
			} catch (ServerLockingException e) {
				
				e.printStackTrace();
				
			} catch (ServerNotActiveException e) {
				
				e.printStackTrace();
			} 
		}
		
		if(result != -1){
			return result;
		}
		
		
		return -1;
	}
	
	public Object GET(String key){
		Object ret = null;
		try {
			InstanceConnection conn = getConnectionContainsKey(key);
			ret = conn.getCommandBuilder().GET(key);
		} catch (RemoteException e) {
			
			e.printStackTrace();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		
		try {
			ByteArrayInputStream in = new ByteArrayInputStream((byte [])ret);
			ObjectInputStream is = new ObjectInputStream(in);
			ret = is.readObject();
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}
		
		
		return ret;
	}
	
	public Boolean EXISTS(String key) {
		
		boolean flag = false;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(key);
			flag = conn.getCommandBuilder().EXISTS(key);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		return flag;
	}
	
	public int EXPIRE(String key, long liveTime) {
		
		int result = -1;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(key);
			result = conn.getCommandBuilder().EXPIRE(key, liveTime);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		if(result != -1){
			return result;
		}
		
		return -1;
	}
	
	public Object DEL(String key) {
		
		Object ret = null;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(key);
			ret = conn.getCommandBuilder().DEL(key);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		try {
			
			ByteArrayInputStream in = new ByteArrayInputStream((byte [])ret);
			ObjectInputStream is = new ObjectInputStream(in);
			
			ret = is.readObject();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}
		
		return ret;
	}
	
	public int PERSIST(String key) {
		
		int ret = -1;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(key);
			ret = conn.getCommandBuilder().PERSIST(key);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		if(ret != -1){
			return ret;
		}
		
		return -1;
	}
	
	public int RENAMENX(String key, String newKey) {
		
		int ret = -1;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(key);
			ret = conn.getCommandBuilder().RENAMENX(key, newKey);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		if(ret != -1){
			return ret;
		}
		
		return -1;
	}
	
	// Hashes ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public int HSET(String parentKey, String key, Object value) {
		
		byte[] ct = null;
		
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ObjectOutputStream os;
			os = new ObjectOutputStream(out);
			os.writeObject(value);
			ct = out.toByteArray();
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		int ret = -1;
		
		if(ct != null){
			
			try {
				
				InstanceConnection conn = getConnectionContainsKey(parentKey);
				
				if(conn != null){
					
					ret = conn.getCommandBuilder().HSET(parentKey, key, ct);
					
				} else{
					
					conn = getMinimumUsageServer();
					ret = conn.getCommandBuilder().HSET(parentKey, key, ct);
				} 
				
				messageManager.insertMessage("Target server: "+conn.getServerInfo().getServerAddr().getHostAddress()+" / key: "+parentKey);
				
			} catch (RemoteException e) {
				
				e.printStackTrace();	
				
			} catch (IOException e) {
				
				e.printStackTrace();	
				
			} catch (ServerLockingException e) {
				
				e.printStackTrace();
				
			} catch (ServerNotActiveException e) {
				
				e.printStackTrace();
			} 
		}
		
		if(ret != -1)
			return ret;
		
		return -1;
	}
	
	public Object HGET(String parentKey, String key) {
		
		Object ret = null;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(parentKey);
			ret = conn.getCommandBuilder().HGET(parentKey, key);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();	
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 		 
		
		try {
			
			ByteArrayInputStream in = new ByteArrayInputStream((byte [])ret);
			ObjectInputStream is = new ObjectInputStream(in);
			
			ret = is.readObject();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}		
		
		return ret;
	}
	
	public boolean HEXISTS(String parentKey, String key) {
				
		boolean flag = false;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(parentKey);
			flag = conn.getCommandBuilder().HEXISTS(parentKey, key);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();	
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 		
				
		return flag;
	}
	
	public Object HDEL(String parentKey, String key) {
		
		Object ret = null;
		// HDEL's return value must be Object, but HDEL's return value is int in CommandBuilder at this time. 
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(parentKey);
			ret = conn.getCommandBuilder().HDEL(parentKey, key);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();	
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		}
		
		try {
			
			ByteArrayInputStream in = new ByteArrayInputStream((byte [])ret);
			ObjectInputStream is = new ObjectInputStream(in);
			
			ret = is.readObject();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}
		
		return ret;
	}
	
	public HashMap<String, Object> HGETALL(String parentKey) {
		
		HashMap<String, Object> ret = null;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(parentKey);
			ret = conn.getCommandBuilder().HGETALL(parentKey);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();	
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		return ret;
	}
	
	public String [] HKEYS(String parentKey) {
		
		String[] ret = null;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(parentKey);
			ret = conn.getCommandBuilder().HKEYS(parentKey);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();		
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 

		return ret;
	}
	
	public int HLEN(String parentKey) {
		
		int ret = -1;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(parentKey);
			ret = conn.getCommandBuilder().HLEN(parentKey);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();		
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 		
		
		if(ret != -1)
			return ret;
		
		return -1;
	}
	
	public int HMSET(String parentKey, HashMap<String, Object> data) {
					
		int ret = -1;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(parentKey);
			
			if(conn != null){
				
				ret = conn.getCommandBuilder().HMSET(parentKey, data);
				
			} else{
				
				conn = getMinimumUsageServer();
				ret = conn.getCommandBuilder().HMSET(parentKey, data);
			}
			
			messageManager.insertMessage("Target server: "+conn.getServerInfo().getServerAddr().getHostAddress()+" / key: "+parentKey);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();		
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		if(ret != -1)
			return ret;
		
		return -1;
	}
	
	public ArrayList<HashMap<String, Object>> HMGET(String parentKey, String [] keys) {
		
		ArrayList<HashMap<String, Object>> alist = null;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(parentKey);
			alist = conn.getCommandBuilder().HMGET(parentKey, keys);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();		
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		return alist;
	}
	
	public int HSETNX(String parentKey, String key) {
		
		int ret = -1;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(parentKey);			
			ret = conn.getCommandBuilder().HSETNX(parentKey, key);			
			
		} catch (RemoteException e) {
			
			e.printStackTrace();		
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		if(ret != -1)
			return ret;
		
		return -1;
	}
	
	// Sorted Sets ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public int ZADD(String parentKey, String key, int value) {
		
		int ret = -1;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(parentKey);			
			ret = conn.getCommandBuilder().ZADD(parentKey, key, value);			
			
		} catch (RemoteException e) {
			
			e.printStackTrace();	
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		if(ret != -1)
			return ret;
		
		return -1;
	}
	
	public int ZCOUNT(String key) {
		
		int ret = -1;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(key);			
			ret = conn.getCommandBuilder().ZCOUNT(key);			
			
		} catch (RemoteException e) {
			
			e.printStackTrace();		
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		if(ret != -1)
			return ret;
		
		return -1;
	}
	
	public String [] ZRANGE(String key, int top, int bottom) {
				
		String[] ret = null;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(key);			
			ret = conn.getCommandBuilder().ZRANGE(key, top, bottom);			
			
		} catch (RemoteException e) {
			
			e.printStackTrace();		
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 		
		
		return ret;
	}
	
	public String [] ZREVRANGE(String key, int top, int bottom) {
		
		String[] ret = null;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(key);			
			ret = conn.getCommandBuilder().ZREVRANGE(key, top, bottom);			
			
		} catch (RemoteException e) {
			
			e.printStackTrace();	
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 		
		
		return ret;
	}
	
	public ArrayList<HashMap<String, Object>> ZRANGEBYSCORE(String key, int top, int bottom) {
		
		ArrayList<HashMap<String, Object>> alist = null;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(key);
			alist = conn.getCommandBuilder().ZRANGEBYSCORE(key, top, bottom);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();	
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		return alist;
	}
	
	public ArrayList<HashMap<String, Object>> ZREVRANGEBYSCORE(String key, int top, int bottom) {
		
		ArrayList<HashMap<String, Object>> alist = null;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(key);
			alist = conn.getCommandBuilder().ZREVRANGEBYSCORE(key, top, bottom);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();	
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		return alist;
	}
	
	public int ZRANK(String parentKey, String key) {
		
		int ret = -1;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(parentKey);			
			ret = conn.getCommandBuilder().ZRANK(parentKey, key);			
			
		} catch (RemoteException e) {
			
			e.printStackTrace();	
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		if(ret != -1)
			return ret;
		
		return -1;
	}
	
	public int ZREVRANK(String parentKey, String key) {
		
		int ret = -1;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(parentKey);			
			ret = conn.getCommandBuilder().ZRANK(parentKey, key);			
			
		} catch (RemoteException e) {
			
			e.printStackTrace();	
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		if(ret != -1)
			return ret;
		
		return -1;
	}
	
	public Object ZREM(String parentKey, String key) {
		
		Object ret = null;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(parentKey);
			ret = conn.getCommandBuilder().ZREM(parentKey, key);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 		
		
		try {
			
			ByteArrayInputStream in = new ByteArrayInputStream((byte [])ret);
			ObjectInputStream is = new ObjectInputStream(in);
			
			ret = is.readObject();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}		
		
		return ret;
	}
	
	public ArrayList<HashMap<String, Object>> ZREMRANGEBYRANK(String parentKey, String key, int top, int bottom) {
		
		ArrayList<HashMap<String, Object>> alist = null;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(parentKey);
			alist = conn.getCommandBuilder().ZREMRANGEBYRANK(parentKey, key, top, bottom);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();		
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		return alist;
	}
	
	public ArrayList<HashMap<String, Object>> ZREMREVRANGEBYRANK(String parentKey, String key, int top, int bottom) {
		
		ArrayList<HashMap<String, Object>> alist = null;
		
		try {
			
			InstanceConnection conn = getConnectionContainsKey(parentKey);
			alist = conn.getCommandBuilder().ZREMREVRANGEBYRANK(parentKey, key, top, bottom);
			
		} catch (RemoteException e) {
			
			e.printStackTrace();		
			
		} catch (ServerLockingException e) {
			
			e.printStackTrace();
			
		} catch (ServerNotActiveException e) {
			
			e.printStackTrace();
		} 
		
		return alist;
	}
	
}
