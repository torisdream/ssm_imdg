<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
<link href="/css/bootstrap.css" rel="stylesheet">
<link href="/css/signin.css" rel="stylesheet">
</head>
<body>
	<div class="container">

		<form class="form-signin" action="/loginProc.do">
			<h2 class="form-signin-heading">Please sign in</h2>
			<input type="text" id="id" name="id" class="form-control" placeholder="Id" autofocus>
			<input type="password" id="pw" name="pw" class="form-control" placeholder="Password">
			<label class="checkbox">
				<input type="checkbox" value="remember-me"> Remember me
			</label>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
		</form>

    </div>
</body>
</html>