package com.hongkong.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

public class MemberController implements Controller {

	@Override
	public ModelAndView handleRequest(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		System.out.println("MemberController Called");
		
		String id = req.getParameter("id");
		
		req.setAttribute("id", id);
		
		ModelAndView mv = new ModelAndView("_member");
		return mv;
	}

}
